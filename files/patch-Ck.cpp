--- Ck.cpp.orig	2018-11-12 21:02:12.000000000 +0100
+++ Ck.cpp	2018-11-12 21:32:31.216698000 +0100
@@ -93,7 +93,7 @@
 
 		vt = *((long *)return_value);
 
-		std::snprintf(device, 32, "/dev/tty%ld", vt);
+		snprintf(device, 32, "/dev/tty%ld", vt - 1);
 
 		if(return_value)
 			XFree(return_value);
